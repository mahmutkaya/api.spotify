@smoke @userData
Feature: User Profile Test

  Scenario: Getting user data
    Given the endpoint to get user data '/me'
    When I make get request to check my profile
    Then the following user data should return:
      | display_name | id      | type | uri      |
      | mahmut       | USER_ID | user | USER_URI |
