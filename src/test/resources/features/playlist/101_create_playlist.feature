@smoke @createPlaylist
Feature: Create Playlist

  Scenario Outline: <test_case> <expected_result> <err_msg>
    Given the endpoint to create a playlist is: '/users/3132jto2jv7iklv7rbbrmfdkzguq/playlists'
    And I want to create a playlist with the following attributes:
      | name   | description   | public   |
      | <name> | <description> | <public> |

    When I save the new playlist '<test_case>'
    Then the save '<expected_result>' '<err_msg>'

    Examples:
      | test_case                | expected_result | name     | description       | public | err_msg                      |
      | WITHOUT NAME             | FAILS           |          | classics playlist | false  | Missing required field: name |
      | WITH ALL REQUIRED FIELDS | IS SUCCESSFUL   | classics | classics playlist | false  |                              |
