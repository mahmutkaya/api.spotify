package runners;


import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "html:test-results/cucumber/cucumber-html-report.html",
                "json:test-results/cucumber/cucumber-json-report.json",
                "rerun:test-results/rerun.txt"
        },
        features = "src/test/resources/features",
        glue = "stepdefinitions",
        tags = "@createPlaylist",
        dryRun = false
)
public class CucumberTestRunner {
}
