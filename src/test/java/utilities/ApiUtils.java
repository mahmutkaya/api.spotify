package utilities;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class ApiUtils {

    private final static String BASE_URI = ConfigReader.getProperty("base_uri");
    private final static String AUTH_URI = ConfigReader.getProperty("auth_uri");
    private final static String GRANT_TYPE = "grant_type";
    private final static String REFRESH_TOKEN = "refresh_token";
    private final static String CLIENT_ID = "client_id";
    private final static String CLIENT_SECRET = "client_secret";
    private final static String ACCESS_TOKEN = "access_token";
    private final static String BEARER = "Bearer";

    private static Response response;

    private static RequestSpecification getRequest() {
        return given()
                .baseUri(BASE_URI)
                .headers(
                        "Accept", ContentType.JSON,
                        "Content-Type", ContentType.JSON,
                        "Authorization", getToken()
                )
                .when();
    }

    private static String getToken() {
        Map<String, String> reqBody = new HashMap<>();
        reqBody.put(GRANT_TYPE, REFRESH_TOKEN);
        reqBody.put(REFRESH_TOKEN, ConfigReader.getProperty(REFRESH_TOKEN));
        reqBody.put(CLIENT_ID, ConfigReader.getProperty(CLIENT_ID));
        reqBody.put(CLIENT_SECRET, ConfigReader.getProperty(CLIENT_SECRET));

        response =
                given()
                        .contentType(ContentType.URLENC)
                        .formParams(reqBody)
                        .when()
                        .post(AUTH_URI);

        String token = response.as(HashMap.class).get(ACCESS_TOKEN).toString();

        return String.format("%s %s", BEARER, token);
    }

    public static Response post(String pathParams, Object reqBody) {
        return response = getRequest().body(reqBody).post(pathParams);
    }

    public static Response get(String pathParams) {
        return response = getRequest().get(pathParams);
    }

    public static Response put(String pathParams, Object reqBody) {
        return response = getRequest().body(reqBody).put(pathParams);
    }

    public static Response delete(String pathParams) {
        return response = getRequest().put(pathParams);
    }
}
