package pojos;

import utilities.ConfigReader;

public class UserProfile {

    private String display_name;
    private String id;
    private String type;
    private String uri;

    public UserProfile() {
    }

    //use this constructor to convert DataTable to Java Object with cucumber DataTableType
    public UserProfile(String display_name, String id, String type, String uri) {
        this.display_name = display_name;
        this.id = ConfigReader.getProperty(id);
        this.type = type;
        this.uri = ConfigReader.getProperty(uri);;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
