package pojos;

public class Playlist {

    private String name;
    private String description;
    private boolean isPublic;

    public Playlist() {
    }

    public Playlist(String name, String description, String isPublic) {
        this.name = name;
        this.description = description;
        this.isPublic = Boolean.parseBoolean(isPublic);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}
