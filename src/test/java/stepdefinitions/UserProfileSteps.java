package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import pojos.UserProfile;
import utilities.ApiUtils;

import java.util.List;

import static org.hamcrest.Matchers.*;

public class UserProfileSteps {

    private Response response;
    private String endpoint;

    @Given("the endpoint to get user data {string}")
    public void the_endpoint_to_get_user_data(String endpoint) {
        this.endpoint = endpoint;
    }

    @When("I make get request to check my profile")
    public void i_make_get_request_to_check_my_profile() {
        response = ApiUtils.get(endpoint);
    }

    @Then("the following user data should return:")
    public void the_following_user_data_should_return(List<UserProfile> userProfileDt) {
        UserProfile userProfile = userProfileDt.get(0);

        response.then()
                .assertThat()
                .body(
                        "display_name", equalTo(userProfile.getDisplay_name()),
                        "id", equalTo(userProfile.getId()),
                        "type", equalTo(userProfile.getType()),
                        "uri", equalTo(userProfile.getUri())
                );
    }
}
