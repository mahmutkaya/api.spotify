package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;
import pojos.Playlist;
import utilities.ApiUtils;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

import java.util.List;

public class PlaylistSteps {
    private Response response;
    private String endpoint;

    private static Playlist playlist;

    @Given("the endpoint to create a playlist is: {string}")
    public void the_endpoint_to_create_a_playlist_is(String endpoint) {
        this.endpoint = endpoint;
    }

    @Given("I want to create a playlist with the following attributes:")
    public void i_want_to_create_a_playlist_with_the_following_attributes(List<Playlist> playlistDt) {
        playlist = playlistDt.get(0);
    }

    @When("I save the new playlist {string}")
    public void i_save_the_new_playlist(String test_case) {
        response = ApiUtils.post(endpoint, playlist);
    }

    @Then("the save {string} {string}")
    public void the_save(String expectedResult, String expectedErrMsg) {
        int statusCode = response.statusCode();

        if (expectedResult.equals("FAILS")) {
            String actualErrMsg = response.body().jsonPath().getString("error.message");

            Assert.assertEquals(400, statusCode);
            Assert.assertEquals(expectedErrMsg, actualErrMsg);
        }
        if (expectedResult.equals("IS SUCCESSFUL")) {
            Assert.assertEquals(201, statusCode);

            response.then().assertThat().body(
                    "name", equalTo(playlist.getName()),
                    "description", equalTo(playlist.getDescription()),
                    "public", equalTo(playlist.isPublic())

            );
        }

    }
}
