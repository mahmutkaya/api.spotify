package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.DataTableType;
import pojos.Playlist;
import pojos.UserProfile;

import java.util.Map;

//convert DataTable to Java Object
public class DataTableTypes {

    @DataTableType
    public UserProfile userProfileEntry(Map<String, String> entry) {
        return new UserProfile(
                entry.get("display_name"),
                entry.get("id"),
                entry.get("type"),
                entry.get("uri")
        );
    }

    @DataTableType
    public Playlist playlistEntry(Map<String, String> entry) {
        return new Playlist(
                entry.get("name"),
                entry.get("description"),
                entry.get("isPublic")
        );
    }

}
