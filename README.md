spotify-api
=================
- API Automation Testing Using Java, serenity, cucumber and restAssured

- api documentation: [developer.spotify](https://developer.spotify.com/documentation)

### dependencies:
- [serenity-core](https://mvnrepository.com/artifact/net.serenity-bdd/serenity-core)
- [serenity-cucumber6](https://mvnrepository.com/artifact/net.serenity-bdd/serenity-cucumber6)
- [serenity-junit](https://mvnrepository.com/artifact/net.serenity-bdd/serenity-junit)
- [rest-assured](https://mvnrepository.com/artifact/io.rest-assured/rest-assured)
- [hamcrest](https://mvnrepository.com/artifact/org.hamcrest/hamcrest)
- [jackson-databind](https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind)

- *sdk version: 1.8*

### Setting Up
These instructions will get you a copy of the project up and running on your local machine.

- *clone the repo:*
```shell
git clone https://gitlab.com/mahmutkaya/api.spotify.git
```
- *create ```configuration.properties``` file at project level*
- *and add the text below into it with replacing your own values*
```properties
base_uri = https://api.spotify.com/v1
refresh_token = <refresh_token>
client_id = <client_id>
client_secret = <client_secret>
```

Running tests from terminal:
```shell
mvn -B verify --file pom.xml
```
